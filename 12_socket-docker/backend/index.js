const express = require('express');
const cors = require('cors');
const path = require('path');
const morgan = require('morgan');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const socketIO = require('socket.io');

dotenv.config({
  path: process.env.NODE_ENV === 'production' ? './envs/production.env' : './envs/development.env',
});

const { serverConfig } = require('./configs');
const { userRouter, authRouter, todoRouter, viewRouter } = require('./routes');
const { errorController } = require('./controllers');

const app = express();

// SETUP PUG TEMPLATE ENGINE ==========
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

// MNGO CONNECT =======================
mongoose
  .connect(serverConfig.mongoUrl)
  .then(() => {
    console.log('Mongo DB connected!');
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });

// MIDDLEWARES =========================
if (serverConfig.environment === 'development') app.use(morgan('dev'));

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

// custom global MW
app.use((req, res, next) => {
  console.log('Hello from middleware!!');

  req.time = new Date().toLocaleString('uk-UA');

  next();
});

// CONTROLLERS ===============================
app.get('/ping', (req, res) => {
  // res.sendStatus(201);
  // res.status(200).send('<h1>Hello from server =^^=</h1>');
  res.status(200).json({
    msg: 'pong!',
    date: req.time,
  });
});

// ROUTES ==========================
const pathPrefix = '/api/v1';

app.use(`${pathPrefix}/auth`, authRouter);
app.use(`${pathPrefix}/users`, userRouter);
app.use(`${pathPrefix}/todos`, todoRouter);
app.use('/', viewRouter);

/**
 * Handle 404 error
 */
app.all('*', (req, res) => {
  res.status(404).json({
    msg: 'Oops! Resource not found.',
  });
});

/**
 * Global error handler. 4 args required!!!!
 */
app.use(errorController.globalErrorHandler);

// SERVER INIT =========================
const port = serverConfig.port ?? 4000;

const server = app.listen(port, () => {
  console.log(`Server is up and running on port ${port}..`);
});

// SOCKET.IO example =====================
const io = socketIO(server);

// Ex.1 basic
// io.on('connection', (socket) => {
//   console.log('Client connected!');

//   socket.emit('message', { msg: 'Hello from socket!!' });

//   socket.on('custom', (data) => {
//     console.log('>>>>>>>>>>>>>>>>>>>>>>>');
//     console.log({ data });
//     console.log('<<<<<<<<<<<<<<<<<<<<<<<');
//   });
// });

// Ex.2 simple chat
// io.on('connection', (socket) => {
//   socket.on('message', (msg) => {
//     io.emit('message', msg);
//   });
// });

// Ex.3 final
const nodeNameSpace = io.of('/nodeNameSpace');

nodeNameSpace.on('connection', (socket) => {
  socket.on('join', (data) => {
    socket.join(data.room);

    const msg = `${data.nick ? '' : 'New user '}joined ${data.room} room`;

    nodeNameSpace.in(data.room).emit('message', { msg, nick: data.nick });
  });

  socket.on('message', (data) => {
    nodeNameSpace.in(data.room).emit('message', { msg: data.msg, nick: data.nick });
  });
});

module.exports = server;
